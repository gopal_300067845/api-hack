<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.myntra.hack</groupId>
    <artifactId>hack-api</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>hack-api</name>
    <description>Myntra Hack Api</description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.9.RELEASE</version>
    </parent>

    <properties>
        <rpm.name>hack-api</rpm.name>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <app.home>/myntra/hack-api/service/releases</app.home>
        <endorsed.dir>${project.build.directory}/endorsed</endorsed.dir>
        <maven.build.timestamp.format>yyyyMMddHHmmss</maven.build.timestamp.format>
        <package.number>${maven.build.timestamp}</package.number>
    </properties>

    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-actuator</artifactId>
        </dependency>

        <!-- web development, including Tomcat and spring-webmvc -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <!-- Spring security -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- spring-data-jpa, spring-orm and Hibernate -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.zaxxer/HikariCP -->
        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
            <version>2.4.0</version>
        </dependency>

        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>1.4.193</version>
        </dependency>

        <!-- spring-test, hamcrest, ... -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>exec-maven-plugin</artifactId>
                <groupId>org.codehaus.mojo</groupId>
                <version>1.6.0</version>
                <executions>
                    <execution>
                        <id>Creating Services File</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                        <configuration>
                            <commandlineArgs>
                                ${basedir}
                            </commandlineArgs>
                            <executable>${basedir}/src/main/configuration/bin/createProperties.sh
                            </executable>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>rpm-maven-plugin</artifactId>
                <version>2.1.5</version>
                <executions>
                    <execution>
                        <id>0</id>
                        <phase>package</phase>
                        <goals>
                            <goal>attached-rpm</goal>
                        </goals>
                        <configuration>
                            <name>${rpm.name}-ops</name>
                            <version>${project.version}${package.number}</version>
                            <group>Applications/Programming</group>
                            <description>Hack Api Service config</description>
                            <autoProvides>false</autoProvides>
                            <autoRequires>false</autoRequires>
                            <mappings>
                                <mapping>
                                    <directory>
                                        ${app.home}/${project.version}${package.number}_1/profiles
                                    </directory>
                                    <filemode>755</filemode>
                                    <username>myntra</username>
                                    <groupname>myntra</groupname>
                                    <sources>
                                        <source>
                                            <location>src/main/configuration/profiles</location>
                                        </source>
                                    </sources>
                                </mapping>
                                <mapping>
                                    <directory>
                                        ${app.home}/${project.version}${package.number}_1/scripts
                                    </directory>
                                    <filemode>755</filemode>
                                    <username>myntra</username>
                                    <groupname>myntra</groupname>
                                    <sources>
                                        <source>
                                            <location>src/main/configuration/scripts</location>
                                        </source>
                                    </sources>
                                </mapping>
                            </mappings>
                        </configuration>
                    </execution>
                    <execution>
                        <id>1</id>
                        <phase>package</phase>
                        <goals>
                            <goal>rpm</goal>
                        </goals>
                        <configuration>
                            <name>${rpm.name}-app</name>
                            <version>${project.version}${package.number}</version>
                            <group>Applications/Programming</group>
                            <description>Hack Api Service config</description>
                            <autoProvides>false</autoProvides>
                            <autoRequires>false</autoRequires>
                            <mappings>
                                <mapping>
                                    <directory>
                                        ${app.home}/${project.version}${package.number}_1/bin
                                    </directory>
                                    <filemode>755</filemode>
                                    <username>myntra</username>
                                    <groupname>myntra</groupname>
                                    <sources>
                                        <source>
                                            <location>src/main/configuration/bin</location>
                                            <excludes>
                                                <exclude>
                                                    createProperties.sh
                                                </exclude>
                                            </excludes>
                                        </source>
                                    </sources>
                                </mapping>
                                <mapping>
                                    <directory>
                                        ${app.home}/${project.version}${package.number}_1/hack-api-tomcat/conf
                                    </directory>
                                    <filemode>755</filemode>
                                    <username>myntra</username>
                                    <groupname>myntra</groupname>
                                    <sources>
                                        <source>
                                            <location>src/main/configuration/conf</location>
                                        </source>
                                    </sources>
                                </mapping>
                                <mapping>
                                    <directory>
                                        ${app.home}/${project.version}${package.number}_1/hack-api-tomcat/webapps
                                    </directory>
                                    <filemode>755</filemode>
                                    <username>myntra</username>
                                    <groupname>myntra</groupname>
                                    <sources>
                                        <source>
                                            <location>
                                                ${project.build.outputDirectory}/../hack-api.jar
                                            </location>
                                        </source>
                                    </sources>
                                </mapping>
                            </mappings>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <finalName>hack-api</finalName>
    </build>
</project>
