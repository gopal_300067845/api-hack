/**
 *
 */
package com.myntra.hack.enums;

public enum ShareModeEnum {

    PRIVATE,
    PUBLIC;

}
