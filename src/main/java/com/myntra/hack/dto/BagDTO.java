package com.myntra.hack.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.myntra.hack.enums.ShareModeEnum;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BagDTO implements Serializable {

	private String id;
	private String[] itemIds;
	private List<String> sharedBags;
	private String owner;
	private String descriptions;
	private ShareModeEnum mode;

	public BagDTO() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String[] getItemIds() {
		return itemIds;
	}

	public void setItemIds(String[] itemIds) {
		this.itemIds = itemIds;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	public ShareModeEnum getMode() {
		return mode;
	}

	public void setMode(ShareModeEnum mode) {
		this.mode = mode;
	}

	public List<String> getSharedBags() {
		return sharedBags;
	}

	public void setSharedBags(List<String> sharedBags) {
		this.sharedBags = sharedBags;
	}
}
