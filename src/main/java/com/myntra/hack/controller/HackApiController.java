package com.myntra.hack.controller;

import com.myntra.hack.common.GenericRes;
import com.myntra.hack.dto.BagDTO;
import com.myntra.hack.dto.ItemDTO;
import com.myntra.hack.exception.ControllerException;
import com.myntra.hack.service.HackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/v0")
public class HackApiController extends ControllerException {

    @Autowired
    private HackService hackService;

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    @ResponseBody
    public GenericRes<List<ItemDTO>> getAllItems(HttpServletRequest request, HttpServletResponse response) {

        return new GenericRes<List<ItemDTO>>(hackService.getAllItems());
    }

    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.GET)
    @ResponseBody
    public GenericRes<ItemDTO> getItemById(@PathVariable("itemId") String itemId, HttpServletRequest request, HttpServletResponse response) {

        return new GenericRes<ItemDTO>(hackService.getItemById(itemId));
    }

    @RequestMapping(value = "/bag/share", method = RequestMethod.POST)
    @ResponseBody
    public GenericRes<BagDTO> shareCart(@Valid @RequestBody BagDTO bagDTO, HttpServletRequest request, HttpServletResponse response) {

        return new GenericRes<BagDTO>(hackService.shareCart(bagDTO));
    }

    @RequestMapping(value = "/bag", method = RequestMethod.GET)
    @ResponseBody
    public GenericRes<BagDTO> getBag(@RequestHeader("userId") String userId, HttpServletRequest request, HttpServletResponse response) {

        return new GenericRes<BagDTO>(hackService.getBag(userId));
    }

    @RequestMapping(value = "/bag/accept", method = RequestMethod.GET)
    @ResponseBody
    public GenericRes<BagDTO> acceptBag(@RequestHeader("userId") String userId, @RequestHeader("bagId") String bagId, HttpServletRequest request, HttpServletResponse response) {

        return new GenericRes<BagDTO>(hackService.acceptBag(userId,bagId));
    }

}
