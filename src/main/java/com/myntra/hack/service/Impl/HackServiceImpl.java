package com.myntra.hack.service.Impl;

import com.myntra.hack.common.HackUtilities;
import com.myntra.hack.dto.BagDTO;
import com.myntra.hack.dto.ItemDTO;
import com.myntra.hack.service.HackService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service("HackService")
public class HackServiceImpl implements HackService {

	@PostConstruct
	@Override
	public void init(){
		HackUtilities.createAllItems();
	}

	@Override
	public List<ItemDTO> getAllItems() {
		return HackUtilities.getAllItems();
	}

	@Override
	public ItemDTO getItemById(String itemId) {
		return HackUtilities.getItemById(itemId);
	}

	@Override
	public BagDTO shareCart(BagDTO bagDTO){

		return HackUtilities.shareBag(bagDTO);
	}

	@Override
	public BagDTO getBag(String userId){

		return HackUtilities.getBag(userId);
	}

	@Override
	public BagDTO acceptBag(String userId, String bagId){

		return HackUtilities.acceptBag(userId,bagId);
	}
}
