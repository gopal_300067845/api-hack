package com.myntra.hack.service;

import com.myntra.hack.dto.BagDTO;
import com.myntra.hack.dto.ItemDTO;

import java.util.List;

public interface HackService {

	void init();

	List<ItemDTO> getAllItems();

	ItemDTO getItemById(String itemId);

	BagDTO shareCart(BagDTO bagDTO);

	BagDTO getBag(String userId);

	BagDTO acceptBag(String userId, String bagId);

}
