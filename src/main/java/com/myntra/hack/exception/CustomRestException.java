package com.myntra.hack.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;


public class CustomRestException extends RuntimeException {
    private static final long serialVersionUID = -977217199574702703L;

    public static int INTERNAL_ERROR_CODE = -1;
    protected int httpErrorCode = SC_OK;
    protected int errorCode = Integer.MAX_VALUE;

    protected String exType;
    protected String exReason;

    protected Map<String, Object> meta;
    protected Map<String, String> dictionary;

    public CustomRestException() {
        super();
        this.meta = new LinkedHashMap<String, Object>();
    }

    public CustomRestException(String message) {
        super(message);
        this.meta = new LinkedHashMap<String, Object>();
    }

    public CustomRestException(String message, Throwable t) {
        super(message, t);
        this.meta = new LinkedHashMap<String, Object>();
    }

    public CustomRestException(String message, Throwable t, int errCode) {
        this(message, t);
        errorCode = errCode;
    }

    public CustomRestException(String message, Throwable t, int errCode, int httpErrCode) {
        this(message, t);
        errorCode = errCode;
        httpErrorCode = httpErrCode;
    }

    public CustomRestException(int errCode, int httpErrCode, String message) {
        this(message);
        errorCode = errCode;
        httpErrorCode = httpErrCode;
    }

    public CustomRestException(int errCode, int httpErrCode, String message, Throwable t) {
        this(message, t);
        errorCode = errCode;
        httpErrorCode = httpErrCode;
    }

    public String getExType() {
        return exType;
    }

    public void setExType(String exType) {
        this.exType = exType;
    }

    public String getExReason() {
        return exReason;
    }

    public void setExReason(String exReason) {
        this.exReason = exReason;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getHttpErrorCode() {
        return httpErrorCode;
    }

    public void setHttpErrorCode(int httpErrorCode) {
        this.httpErrorCode = httpErrorCode;
    }

    public Map<String, Object> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, Object> meta) {
        this.meta = meta;
    }

    public Map<String, String> getDictionary() {
        return dictionary;
    }

    public void setDictionary(Map<String, String> dictionary) {
        this.dictionary = dictionary;
    }

    /* Exceptions */
    public static final class ExasolQueryExecutionFail extends CustomRestException {
        private static final long serialVersionUID = 1L;

        public ExasolQueryExecutionFail(Exception e) {
            super(40001, SC_INTERNAL_SERVER_ERROR, String.format("Exasol Query Execution failed {" + e + "}"));
        }
    }

    public static final class ExasolConnectionFail extends CustomRestException {
        private static final long serialVersionUID = 1L;

        public ExasolConnectionFail() {
            super(40002, SC_INTERNAL_SERVER_ERROR, String.format("Service connection to  Exasol Failed"));
        }
    }

    public static final class ExasolExportFileNotAvailable extends CustomRestException {
        private static final long serialVersionUID = 1L;

        public ExasolExportFileNotAvailable(String downloadFilePath) {
            super(40003, SC_INTERNAL_SERVER_ERROR, String.format("Exasol export file not available on mentioned path { " + downloadFilePath + " }"));
        }
    }

    public static final class AzureDownloadFileNotUploaded extends CustomRestException {
        private static final long serialVersionUID = 1L;

        public AzureDownloadFileNotUploaded(String downloadFilePath, Exception e) {
            super(40004, SC_INTERNAL_SERVER_ERROR, String.format("Azure download file not uploaded on mentioned path { " + downloadFilePath + " } with exception [" + e + "]"));
        }
    }

    public static final class AzureFileUploadFailed extends CustomRestException {
        private static final long serialVersionUID = 1L;

        public AzureFileUploadFailed() {
            super(40005, SC_INTERNAL_SERVER_ERROR, String.format("Azure upload file failed......[may be azure intermediate issue]"));
        }

        public AzureFileUploadFailed(Exception e) {
            super(40005, SC_INTERNAL_SERVER_ERROR, String.format("Azure upload file failed with exception { " + e + " }"));
        }
    }

    public static final class ServiceAzureConnectionFailed extends CustomRestException {
        private static final long serialVersionUID = 1L;

        public ServiceAzureConnectionFailed() {
            super(40006, SC_INTERNAL_SERVER_ERROR, String.format("Service not able to connect to azure Blob. Please check credentials...."));
        }
    }

    public static final class FolderCreationOnLocalVmFailed extends CustomRestException {
        private static final long serialVersionUID = 1L;

        public FolderCreationOnLocalVmFailed(Exception e) {
            super(40007, SC_INTERNAL_SERVER_ERROR, String.format("Folder creation failed on local server for exasol export path { " + e + " }"));
        }
    }

    public static final class CreatingConnectionPoolFailedException extends CustomRestException{
        private static final long serialVersionUID = 1L;

        public CreatingConnectionPoolFailedException(){
            super(40008, SC_INTERNAL_SERVER_ERROR, String.format("Error Creating Connection Pool"));
        }

        public CreatingConnectionPoolFailedException(Exception e){
            super(40008, SC_INTERNAL_SERVER_ERROR, String.format("Error Creating Connection Pool. Exception: { " + e + " }" ));
        }

    }

    public static final class InitializingDataPoolFailedException extends CustomRestException{
        private static final long serialVersionUID = 1L;

        public InitializingDataPoolFailedException(Exception e){
            super(40009, SC_INTERNAL_SERVER_ERROR, String.format("Error initializing Data Pool. Exception: { " + e + " }"));
        }
    }

    public static final class InitializingDbPropertiesFailedException extends CustomRestException{
        private static final long serialVersionUID = 1L;

        public InitializingDbPropertiesFailedException(Exception e){
            super(40010, SC_INTERNAL_SERVER_ERROR, String.format("Error initializing Database Properties. Exception: { " + e + " }"));
        }
    }

    public static final class PoolInitializationFailedException extends CustomRestException{
        private static final long serialVersionUID = 1L;

        public PoolInitializationFailedException(Exception e){
            super(40011, SC_INTERNAL_SERVER_ERROR, String.format("Pool Initialization Failed. Exception: { " + e + " }"));
        }

    }

}
