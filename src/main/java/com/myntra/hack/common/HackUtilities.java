package com.myntra.hack.common;

import com.myntra.hack.dto.BagDTO;
import com.myntra.hack.dto.ItemDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class HackUtilities {

	private static Logger logger = LoggerFactory.getLogger(HackUtilities.class);

	public static Map<String, BagDTO> cartMap=new HashMap<>();
	public static Map<String, ItemDTO> itemMap =new HashMap<>();

	public static Map<String, List<String>> bagSharedMap =new HashMap<>();

	public static String[] itemIds = {"0000-0000-0000-0000","1111-1111-1111-1111","2222-2222-2222-2222","3333-3333-3333-3333","4444-4444-4444-4444",
			"5555-5555-5555-5555", "6666-6666-6666-6666", "7777-7777-7777-7777","8888-8888-8888-8888","9999-9999-9999-9999"};

	public static BagDTO shareBag(BagDTO bagDTO){

		String bagId = UUID.randomUUID().toString();
		bagDTO.setId(bagId);
		String userId=bagDTO.getOwner();
		acceptBag(userId,bagId);
		cartMap.put(bagId, bagDTO);
		return bagDTO;
	}

	public static ItemDTO getItemById(String itemId){
		return itemMap.get(itemId);
	}

	public static BagDTO getBag(String userId){

		BagDTO bagDTO = new BagDTO();
		String[] ids= userId.equals("8147504866")?Arrays.copyOfRange(itemIds,0,4):Arrays.copyOfRange(itemIds,5,9);
		bagDTO.setSharedBags(bagSharedMap.get(userId));
		bagDTO.setItemIds(ids);
		return bagDTO;
	}

	public static BagDTO acceptBag(String userId,String bagId){

		List<String> bagIds=new ArrayList<>();
		if(!bagSharedMap.containsKey(userId)){
			bagIds=new ArrayList<>();
			bagIds.add(bagId);
		}else{
			bagIds=bagSharedMap.get(userId);
			bagIds.add(bagId);
		}
		bagSharedMap.put(userId,bagIds);
		return getBag(userId);
	}

	public static void createAllItems(){
		ItemDTO item0 = new ItemDTO("0000-0000-0000-0000");
		itemMap.put(item0.getId(),item0);

		ItemDTO item1 = new ItemDTO("1111-1111-1111-1111");
		itemMap.put(item1.getId(),item1);

		ItemDTO item2 = new ItemDTO("2222-2222-2222-2222");
		itemMap.put(item2.getId(),item2);

		ItemDTO item3 = new ItemDTO("3333-3333-3333-3333");
		itemMap.put(item3.getId(),item3);

		ItemDTO item4 = new ItemDTO("4444-4444-4444-4444");
		itemMap.put(item4.getId(),item4);

		ItemDTO item5 = new ItemDTO("5555-5555-5555-5555");
		itemMap.put(item5.getId(),item5);

		ItemDTO item6 = new ItemDTO("6666-6666-6666-6666");
		itemMap.put(item6.getId(),item6);

		ItemDTO item7 = new ItemDTO("7777-7777-7777-7777");
		itemMap.put(item7.getId(),item7);

		ItemDTO item8 = new ItemDTO("8888-8888-8888-8888");
		itemMap.put(item8.getId(),item8);

		ItemDTO item9 = new ItemDTO("9999-9999-9999-9999");
		itemMap.put(item9.getId(),item9);
	}
	public static List<ItemDTO> getAllItems(){

		return new ArrayList(itemMap.values());
	}

	public ItemDTO getItem(String itemId){
		if(!itemMap.containsKey(itemId)){
			return null;
		}
		return itemMap.get(itemId);
	}
}
