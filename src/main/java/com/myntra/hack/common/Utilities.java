package com.myntra.hack.common;

import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Utilities {

    private static Logger logger = LoggerFactory.getLogger(Utilities.class);

    public static String removeContainerNamePath(String blobDirPath) {

        int idx = blobDirPath.indexOf('/');
        return blobDirPath.substring(idx + 1, blobDirPath.length());
    }

    public static Pair<String, String> getDirFromFilePath(String downloadFilePath) {

        String[] paths = downloadFilePath.split("/");
        int fileLength = paths[paths.length - 1].length();

        return new Pair<String, String>(downloadFilePath.substring(0, downloadFilePath.length() - fileLength - 1), paths[paths.length - 1]);
    }

    /**
     *  Check file exists and permissions for read
     */
    public static Boolean isFileExists(String fileName) {

        File f = new File(fileName);
        if (f.exists()) {
            return true;
        }
        return false;
    }

    /**
     *  Check dir path and create if not there
     */
    public static void createOrRetrieve(final String target) throws Exception {

        final Path path = Paths.get(target);
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        logger.info("Exported file path created !");
    }

    public static String getDatePath() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDateTime dateDirNamePath = LocalDateTime.now();
        long epocTime = new Date().getTime();
        return dtf.format(dateDirNamePath) + "/" + epocTime;
    }

    public static Boolean deletFile(String filePath) {

        File file = new File(filePath);
        return file.delete();
    }

    public static String modifyFilePath(String blobDirPath) {

        int slashIdx = blobDirPath.indexOf(Constants.SLASH);
        return blobDirPath.substring(slashIdx + 1, blobDirPath.length());
    }
}
