package com.myntra.hack.common;

public interface Constants {

    String BASEPATH = "/myntra/udpexasolunload";
    String FILE_FIELD = "FILE";
    String DOLLER_FIELD = "$";
    String CSV_FORMATE = ".csv";
    String SLASH = "/";

    String UDP_EXASOL = "UDP_EXASOL";

    String CREATING_CONNECTION_POOL_EXCEPTION_MSG = "ERROR CREATING CONNECTION POOL";
    String INITIALSING_DATA_POOL_EXCEPTION_MSG = "ERROR INITIALISING DATA POOL";
}
