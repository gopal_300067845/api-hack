#!/bin/sh

export PATH=$PATH:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/myntra/bin
APP_HOME=/myntra/exasolexport/service/releases/current/exasolexport-tomcat
mkdir -p $APP_HOME/logs
pid=`ps -ef |grep exasolexport-api.jar | grep -v grep |awk '{ print $2 }'`
cd $APP_HOME/logs

echo "Process id is:" $pid
echo "UserName: "
whoami
if [ ! -z $pid ]
then
        status=`kill -9 $pid`
        sleep 5
        echo "Process killed successfully."
        java -jar $APP_HOME/webapps/exasolexport-api.jar --spring.config.location=$APP_HOME/conf/application.properties 2>&1 &
        echo "Process started"
        exit 0
else
        echo "Process is not running, starting process"
        java -jar $APP_HOME/webapps/exasolexport-api.jar --spring.config.location=$APP_HOME/conf/application.properties 2>&1 &
        echo "Process started"
        exit 0
fi
whoami