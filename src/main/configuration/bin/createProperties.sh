#!/bin/sh

rm -rf $1/src/main/configuration/profiles/local/*
rm -rf $1/src/main/configuration/profiles/prod/*
rm -rf $1/src/main/configuration/profiles/prod1/*

mkdir -p $1/src/main/configuration/profiles/local
mkdir -p $1/src/main/configuration/profiles/prod
mkdir -p $1/src/main/configuration/profiles/prod1

python /myntra/myntra-ops/releases/current/bin/yamlConfigReader.py --env 'local' --yamldir $1/src/main/configuration/yml-files --tpldir $1/src/main/configuration/yml-files --outputdir $1/src/main/configuration/profiles/local

python /myntra/myntra-ops/releases/current/bin/yamlConfigReader.py --env 'prod' --yamldir $1/src/main/configuration/yml-files --tpldir $1/src/main/configuration/yml-files --outputdir $1/src/main/configuration/profiles/prod

python /myntra/myntra-ops/releases/current/bin/yamlConfigReader.py --env 'prod1' --yamldir $1/src/main/configuration/yml-files --tpldir $1/src/main/configuration/yml-files --outputdir $1/src/main/configuration/profiles/prod1
