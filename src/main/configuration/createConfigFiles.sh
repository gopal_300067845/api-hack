#!/bin/sh

echo "Removing old(if any) config files...."
rm $2/statsd.properties
rm $2/serviceurls.properties*
rm $2/rollback-dbconfig.properties
rm $2/queue-config.properties
rm $2/oms-dbconfig.properties

echo "Creating the config files......"
python /myntra/myntra-ops/releases/current/bin/yamlConfigReader.py --env $1 --yamldir $2 --tpldir $2 --outputdir $2
