#!/bin/sh

if [ -f /var/www/html/tester.php ]
then
    sed -i "s/$sendMails = true/$sendMails = false/g" /var/www/html/tester.php
    sed -i 's/240/server:7040/g' /var/www/html/tester.php
fi