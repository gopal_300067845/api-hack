SRC_DIR=$1
TARGET_DIR=$2
echo "Copying from OPS_DIR Located at $SRC_DIR"
echo "Copying to   APP_DIR Located at $TARGET_DIR"
cp -a $SRC_DIR/.  $TARGET_DIR/exasolexport-tomcat/conf/
mkdir -p $TARGET_DIR/exasolexport-tomcat/logs
mkdir -p $TARGET_DIR/exasolexport-tomcat/logs/application
mkdir -p $TARGET_DIR/exasolexport-tomcat/temp
mkdir -p $TARGET_DIR/exasolexport-tomcat/lib
